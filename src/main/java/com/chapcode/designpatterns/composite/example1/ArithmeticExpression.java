package com.chapcode.designpatterns.composite.example1;

import java.util.Objects;

public interface ArithmeticExpression {
	double calculate();
}

class NumberExpression implements ArithmeticExpression {
	private double _number;

	public NumberExpression(double number) {
		this._number = number;
	}

	@Override
	public double calculate() {
		return this._number;
	}

}

class SumExpression implements ArithmeticExpression {
	private ArithmeticExpression _left;
	private ArithmeticExpression _right;

	public SumExpression(ArithmeticExpression left, ArithmeticExpression right) {
		Objects.requireNonNull(left);
		Objects.requireNonNull(right);
		
		this._left = left;
		this._right = right;
	}
	
	@Override
	public double calculate() {
		return this._left.calculate() + this._right.calculate();
	}
}

class SubtractExpression implements ArithmeticExpression {
	private ArithmeticExpression _left;
	private ArithmeticExpression _right;

	public SubtractExpression(ArithmeticExpression left, ArithmeticExpression right) {
		Objects.requireNonNull(left);
		Objects.requireNonNull(right);
		
		this._left = left;
		this._right = right;
	}
	
	@Override
	public double calculate() {
		return this._left.calculate() - this._right.calculate();
	}
}

class MultiplyExpression implements ArithmeticExpression {
	private ArithmeticExpression _left;
	private ArithmeticExpression _right;

	public MultiplyExpression(ArithmeticExpression left, ArithmeticExpression right) {
		Objects.requireNonNull(left);
		Objects.requireNonNull(right);
		
		this._left = left;
		this._right = right;
	}
	
	@Override
	public double calculate() {
		return this._left.calculate() * this._right.calculate();
	}
}

class DivideExpression implements ArithmeticExpression {
	private ArithmeticExpression _left;
	private ArithmeticExpression _right;

	public DivideExpression(ArithmeticExpression left, ArithmeticExpression right) {
		Objects.requireNonNull(left);
		Objects.requireNonNull(right);
		
		this._left = left;
		this._right = right;
	}
	
	@Override
	public double calculate() {
		return this._left.calculate() / this._right.calculate();
	}
}
