package com.chapcode.designpatterns.composite.example1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ArithmeticExpressionTest {
	@Test
	public void testNumberExpressionShouldReturnItsNumber() {
		ArithmeticExpression numberOne = new NumberExpression(2);
		assertEquals(2, numberOne.calculate(), 0);
	}
	
	@Test
	public void testSumExpressionContainingTwoNumbersShouldReturnSumOfThem_One() {
		var left = new NumberExpression(2);
		var right= new NumberExpression(3);
		SumExpression sum = new SumExpression(left, right);
		assertEquals(5, sum.calculate(), 0);
	}
	
	@Test
	public void testSumExpressionContainingTwoNumbersShouldReturnSumOfThem_Two() {
		var left = new NumberExpression(-2);
		var right= new NumberExpression(3);
		SumExpression sum = new SumExpression(left, right);
		assertEquals(1, sum.calculate(), 0);
	}
	
	@Test
	public void testSubtractExpressionContainingTwoNumbersShouldReturnDifferenceOfThem_One() {
		var left = new NumberExpression(2);
		var right= new NumberExpression(3);
		var expression = new SubtractExpression(left, right);
		assertEquals(-1, expression.calculate(), 0);
	}
	
	@Test
	public void testSubtractExpressionContainingTwoNumbersShouldReturnDifferenceOfThem_Two() {
		var left = new NumberExpression(-2);
		var right= new NumberExpression(3);
		var expression = new SubtractExpression(left, right);
		assertEquals(-5, expression.calculate(), 0);
	}
	
	@Test
	public void testSumExpressionContainingTwoSumsShouldReturnTotalSum() {
		// (2 + 1) + (2 + 3)
		SumExpression sumOne = new SumExpression(new NumberExpression(2), new NumberExpression(1));
		SumExpression sumTwo = new SumExpression(new NumberExpression(2), new NumberExpression(3));
		
		SumExpression totalSum = new SumExpression(sumOne, sumTwo);
		assertEquals(8, totalSum.calculate(), 0);
	}
	
	@Test
	public void testMultiplyExpressionContainingTwoNumbersShouldReturnMultiplication() {
		MultiplyExpression multiplyExpression = new MultiplyExpression(new NumberExpression(2), new NumberExpression(3));
		assertEquals(6, multiplyExpression.calculate(), 0);
	}
	
	@Test
	public void testMultiplyAndSumExpressionShouldPreserveOrderOfCalculations_One() {
		// 2 + 3 * 5
		var sumLeft = new NumberExpression(2);
		var sumRight = new MultiplyExpression(new NumberExpression(3), new NumberExpression(5));
		SumExpression rootExpression = new SumExpression(sumLeft, sumRight);
		
		assertEquals(17, rootExpression.calculate(), 0);
	}
	
	@Test
	public void testMultiplyAndSumExpressionShouldPreserveOrderOfCalculations_Two() {
		// (2 + 3) * 5
		var left = new SumExpression(new NumberExpression(2), new NumberExpression(3));
		var right = new NumberExpression(5);
		ArithmeticExpression rootExpression = new MultiplyExpression(left, right);
		
		assertEquals(25, rootExpression.calculate(), 0);
	}
	
	@Test
	public void testDivideExpressionContainingTwoNumbersShouldReturnDivision() {
		var expression = new DivideExpression(new NumberExpression(6), new NumberExpression(2));
		assertEquals(3, expression.calculate(), 0);
	}
	
	@Test
	public void testDivideAndSumExpressionShouldPreserveOrderOfCalculations_One() {
		// 2 + 6 / 2
		var sumLeft = new NumberExpression(2);
		var sumRight = new DivideExpression(new NumberExpression(6), new NumberExpression(2));
		var rootExpression = new SumExpression(sumLeft, sumRight);
		
		assertEquals(5, rootExpression.calculate(), 0);
	}
	
	@Test
	public void testDivideAndSumExpressionShouldPreserveOrderOfCalculations_Two() {
		// (2 + 6) / 2
		var divideLeft = new SumExpression(new NumberExpression(2), new NumberExpression(6));
		var divideRight = new NumberExpression(2);
		var rootExpression = new DivideExpression(divideLeft, divideRight);
		
		assertEquals(4, rootExpression.calculate(), 0);
	}
}
